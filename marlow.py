import tkinter as Tk
from tkinter import ttk

import vlc


class Player(Tk.Frame):
    def __init__(self, parent, title='test player'):
        Tk.Frame.__init__(self, parent)

        self.parent = parent

        self.player = None
        self.videopanel = ttk.Frame(self.parent)
        self.canvas = Tk.Canvas(self.videopanel).pack(fill=Tk.BOTH,expand=1)
        self.videopanel.pack(fill=Tk.BOTH,expand=1)

        self.Instance = vlc.Instance()
        self.player = self.Instance.media_player_new()
        self.player.set_xwindow(self.GetHandle())

        self.Media = self.Instance.media_new('../../trailer_480p.mov')
        self.player.set_media(self.Media)
        self.player.play()

    def GetHandle(self):
        return self.videopanel.winfo_id()

if __name__ == "__main__":
    # Create a Tk.App(), which handles the windowing system event loop
    root = Tk.Tk()
    player = Player(root)
    root.mainloop()
